var triggerMusic = function (musicSystem, musicState, tabId) {
    if (musicSystem) {
        if (musicSystem == MusicSystem.Vk) {
            if (musicState == PlayState.Pause || musicState == PlayState.Play)
                $('#head_play_btn').click();
            else if (musicState == PlayState.Back) {
                $('#gp_info').click();
                $('#pd_prev').click();
            }
            else if (musicState == PlayState.Forward) {
                $('#gp_info').click();
                $('#pd_next').click();
            }
        }
        else if (musicSystem == MusicSystem.NewVk) {
            if (musicState == PlayState.Pause || musicState == PlayState.Play)
                $('#top_audio_player .top_audio_player_play').click();
            else if (musicState == PlayState.Back) {
                $('#top_audio_player .top_audio_player_prev').click();
            }
            else if (musicState == PlayState.Forward) {
                $('#top_audio_player .top_audio_player_next').click();
            }
        }
        else if (musicSystem == MusicSystem.Yandex) {
            if (musicState == PlayState.Pause)
                $('.player-controls .player-controls__btn_play').click();
            else if (musicState == PlayState.Play)
                $('.player-controls .player-controls__btn_pause').click();
            else if (musicState == PlayState.Back)
                $('.player-controls .player-controls__btn_prev').click();
            else if (musicState == PlayState.Forward)
                $('.player-controls .player-controls__btn_next').click();

        }
        var request = {msgType: "triggered"};
        request.tabId = tabId;
        chrome.runtime.sendMessage(request);
    }
};