var states = [];
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

        if (!request.msgType || request.msgType != 'music')
            return;

        if (!request.state)
            return;

        states[sender.tab.id] = request;
    });

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

        if (!request.msgType || request.msgType != 'triggered')
            return;

        if (!request.tabId)
            return;

        var tabId = sender.tab.id;
        var state = states[tabId];
        if (!state)
            return;

        var info = state.artist;
        info = info + (state.artist || state.title ? ' - ' : '');
        info = info + state.title;

        chrome.tabs.executeScript(request.tabId, {code: 'var musicInfo = "' + info + '";' + 'showMusicInfoPopup(musicInfo);'});
    });


var lastTabId = 0;
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

        if (!request.msgType || request.msgType != 'keyPressed')
            return;

        if (!request.which)
            return;

        var playedTabId = null;

        if (states.length <= 0)
            return;

        var i=0;
        var someTabWasPlayed = false;
        for (var key in states) {
            var state = states[key];
            if (i == 0 || state.state == PlayState.Play){
                playedTabId = key;
            }
            if (state.state == PlayState.Play){
                someTabWasPlayed = true;
            }
            i++;
        }

        if (!someTabWasPlayed && states[lastTabId]){
            playedTabId = lastTabId;
        }

        var musicSystem = states[playedTabId].system;
        var playState = states[playedTabId].state;

        var which = request.which;
        if (which == 39){ //->
           playState = PlayState.Forward;
        }else if (which == 37){//<-
            playState = PlayState.Back;
        }else if (which == 38 || which == 40){// *up down*
            playState = playState;
        }
        else{
            return;
        }

        var tabId = parseInt(playedTabId);
        lastTabId = tabId;
        chrome.tabs.executeScript(tabId, {file: "constants.js"});
        chrome.tabs.executeScript(tabId, {code: 'triggerMusic('+ musicSystem +', ' + playState + ', ' + sender.tab.id + ')'});
    });

chrome.tabs.query( {} ,function (tabs) {
    for (var i = 0; i < tabs.length; i++) {
        chrome.tabs.executeScript(tabs[i].id, {file: "jquery.js"});
        chrome.tabs.executeScript(tabs[i].id, {file: "constants.js"});
        chrome.tabs.executeScript(tabs[i].id, {file: 'contentKeyPressed.js'});
        chrome.tabs.executeScript(tabs[i].id, {file: 'showInfoPopup.js'});
        chrome.tabs.executeScript(tabId, {file: "trigger.js"});
    }
});

chrome.tabs.onUpdated.addListener(function (tabId){
    chrome.tabs.executeScript(tabId, {file: "jquery.js"});
    chrome.tabs.executeScript(tabId, {file: "constants.js"});
    chrome.tabs.executeScript(tabId, {file: 'contentKeyPressed.js'});
    chrome.tabs.executeScript(tabId, {file: "content.js"});
    chrome.tabs.executeScript(tabId, {file: "showInfoPopup.js"});
    chrome.tabs.executeScript(tabId, {file: "trigger.js"});
});

chrome.tabs.query( {url:supprotedUrls} ,function (tabs) {
    for (var i = 0; i < tabs.length; i++) {
        chrome.tabs.executeScript(tabs[i].id, {file: "content.js"});
    }
});


