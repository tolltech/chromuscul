var imgClickHandler = function (evt) {
    var src = $(evt.currentTarget);
    var tabId = src.attr('tabId');
    var playState = src.attr('playState');
    var musicSystem = src.attr('musicSystem');
    if (!tabId || !playState)
        return;

    tabId = parseInt(tabId);
    chrome.tabs.executeScript(tabId, {file: "constants.js"});
    chrome.tabs.executeScript(tabId, {code: 'triggerMusic('+ musicSystem +', ' + playState +')'});
};

createImg = function(icoPath, tabId, state, system, imgClass){
    var i =  $('<div style="float:left;cursor:pointer;"><img style="width:45px;" class="'+ imgClass +'" src="' + icoPath + '"></img></div>');
    i.attr('tabId', tabId);
    i.attr('playState', state);
    i.attr('musicSystem', system);
    i.attr('class', imgClass);
    i.click(imgClickHandler);
    return i;
};

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

        if (!request.msgType || request.msgType != 'music')
            return;

        if (!request.state)
            return;

        var div = $('#tabid_' + sender.tab.id).length > 0 ? $('#tabid_' + sender.tab.id) : null;
        var img = null;
        var infoDiv = null;

        if (div) {
            img = $('#tabid_' + sender.tab.id + ' img.play');
            infoDiv = $('#tabid_' + sender.tab.id + ' div.songInfo');
        }
        else {
            div = $('<div style="margin: 2px"></div>');
            div.attr('id', 'tabid_' + sender.tab.id);
            div.attr('tabId', sender.tab.id);
            div.attr('class', 'song');

            img = createImg("icons/play.png", sender.tab.id, request.state, request.system, 'play');

            var backDiv = createImg("icons/back.png", sender.tab.id, PlayState.Back, request.system, 'back');
            div.append(backDiv);
            div.append(img);
            var fwDiv = createImg("icons/fw.png", sender.tab.id, PlayState.Forward, request.system, 'fw');
            div.append(fwDiv);

            infoDiv = $('<div style="padding-top: 13px;padding-bottom: 13px;padding-left: 60px;font-size: medium;" class="songInfo"></div>');
            div.append(infoDiv);

            var tabs = [];
            var divs = $('#chContainer .song');
            for (var i = 0; i < divs.length; ++i) {
                tabs.push(parseInt($(divs[i]).attr('tabId')));
            }

            tabs.sort();
            if (tabs.length <= 0 || tabs[tabs.length - 1] < sender.tab.id) {
                var container = $('#chContainer');
                container.append(div);
            }
            else {
                var tabIdToInsert = sender.tab.id;
                if ($('#tabid_' + tabIdToInsert).length <= 0) {
                    var tId = tabs[0];
                    for (var i = 0; i < tabs.length; ++i) {
                        tId = tabs[i];
                        if (tabs[i] >= sender.tab.id)
                            break;
                    }
                    $('#tabid_' + tId).before(div);
                }
            }
        }

        div.attr('title', sender.tab.title + '(' + sender.tab.url + ')');
        div.attr('state', request.state);

        var ico = request.state == PlayState.Play ? 'stop' : 'play';
        img.attr('src', 'icons/' + ico + '.png');

        var info = request.artist;
        info = info + (request.artist || request.title ? ' - ' : '');
        info = info + request.title;
        infoDiv.html(info ? info : sender.tab.title);
    });

$(document).ready(function(){
    askInfo();

    $('body').keyup(function (evt) {
        if (!evt.shiftKey || !evt.ctrlKey || !evt.altKey)
            return;

        var which = evt.which;
        if (which != 38 && which != 39 && which != 37 && which != 40)
            return;

        var request = {msgType: "keyPressed"};
        request.which = which;
        chrome.runtime.sendMessage(request);
    });
});

function askInfo(){
    chrome.tabs.query( { url:supprotedUrls} ,function (tabs) {
        var container = $('#chContainer');
        for (var i = 0; i < tabs.length; i++) {
            chrome.tabs.executeScript(tabs[i].id, {file: "content.js"});
        }
    });
}
