var sendKeyPressed = function (evt) {
    if (!evt.shiftKey || !evt.ctrlKey || !evt.altKey)
        return;

    var which = evt.which;
    if (which != 38 && which != 39 && which != 37 && which != 40)
        return;

    var request = {msgType: "keyPressed"};
    request.which = which;
    chrome.runtime.sendMessage(request);
};

if (typeof wasinjected == 'undefined') {
    $(window).keyup(sendKeyPressed);
}
var wasinjected = true;