var request = {msgType: "music"};

var sendAskInfoResponse = function () {
    var toSend = true;
    var url = window.location.host;
    if (url == 'vk.com') {
        request.system = MusicSystem.Vk;
        var wrap = $('#gp_wrap');
        if (wrap.length <= 0) {
            request.state = PlayState.Pause;
            request.title = '';
            request.artist = '';
        }
        else {
            request.title = $('#gp_wrap #gp_title').html();
            request.artist = $('#gp_wrap #gp_performer').html();
            request.state = $('#gp_wrap #gp_play').hasClass('playing') ? PlayState.Play : PlayState.Pause;
        }
    }
    else if (url == 'new.vk.com') {
        request.system = MusicSystem.NewVk;
        var wrap = $('#top_audio_player');
        if (wrap.length <= 0) {
            request.state = PlayState.Pause;
            request.title = '';
            request.artist = '';
        }
        else {
            request.title = $('#top_audio_player .top_audio_player_title').html();
            request.artist = '';//$('#top_audio_player #gp_performer').html();
            request.state = $('#top_audio_player').hasClass('top_audio_player_playing') ? PlayState.Play : PlayState.Pause;
        }
    }
    else if (url == 'music.yandex.ru') {
        request.system = MusicSystem.Yandex;
        var wrap = $('.player-controls');
        if (wrap.length <= 0) {
            request.state = PlayState.Pause;
            request.title = '';
            request.artist = '';
        }
        else {
            request.title = $('.player-controls .track__name-wrap a').html();
            request.artist = $('.player-controls .track__artists a').html();
            request.state = $('.player-controls .player-controls__btn_pause').length > 0 ? PlayState.Play : PlayState.Pause;
        }
    }
    else
        toSend = false;

    if (toSend) {
        chrome.runtime.sendMessage(request);
        setTimeout(sendAskInfoResponse, 500);
    }
};

if (typeof wascontentinjected == 'undefined') {
    setTimeout(sendAskInfoResponse, 500);
}
var wascontentinjected = true;